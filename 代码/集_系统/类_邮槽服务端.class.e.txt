﻿.版本 2

.程序集 类_邮槽服务端, , 公开, http://msdn.microsoft.com/en-us/library/windows/desktop/aa365147(v=vs.85).aspx
.程序集变量 hSlot, , , , 本类源码来自于SoftApiModule模块

.子程序 _初始化, , , 当基于本类的对象被创建后，此方法会被自动调用
    

.子程序 _销毁, , , 当基于本类的对象被销毁前，此方法会被自动调用
    CloseHandle (hSlot)

.子程序 创建, 逻辑型, 公开, 创建成功返回真，否则返回假。
    .参数 邮槽名称, 文本型

    hSlot ＝ CreateMailslot (“\\.\mailslot\” ＋ 邮槽名称, 0, -1, 0) ' MAILSLOT_WAIT_FOREVER
    返回 (hSlot ≠ 0)

.子程序 关闭, 逻辑型, 公开, 关闭邮槽服务器。
    返回 (CloseHandle (hSlot))

.子程序 读数据, 逻辑型, 公开, 只能在服务器端从邮槽中取出数据。读取成功返回真，否则返回假。
    .参数 数据, 字节集, 参考, 提供参数数据时只能提供变量。从向邮槽中读出的数据。
    .局部变量 Status
    .局部变量 cbMessage, 整数型
    .局部变量 cMessage, 整数型
    .局部变量 lpszBuffer
    .局部变量 cbRead, 整数型
    .局部变量 bool, 逻辑型

    .如果真 (GetMailslotInfo (hSlot, 0, cbMessage, cMessage, 0))
        .如果真 (cbMessage ≠ -1)
            lpszBuffer ＝ HeapAlloc (GetProcessHeap (), 8, 1000)
            bool ＝ ReadFile_整数型 (hSlot, lpszBuffer, cbMessage, cbRead, 0)
            数据 ＝ 指针到字节集 (lpszBuffer, cbRead)
            HeapFree (GetProcessHeap (), 8, lpszBuffer)
        .如果真结束
        返回 (bool)
    .如果真结束
    返回 (假)
    

