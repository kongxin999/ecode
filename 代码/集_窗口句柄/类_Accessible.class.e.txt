﻿.版本 2

.程序集 类_Accessible, , 公开, 感谢会员【酷宝贝】提供Accessible接口
.程序集变量 m_parent, 对象
.程序集变量 m_self, 对象
.程序集变量 m_id, 整数型

.子程序 _初始化, , , 当基于本类的对象被创建后，此方法会被自动调用
    

.子程序 _销毁, , , 当基于本类的对象被销毁前，此方法会被自动调用
    

.子程序 创建自窗口句柄, 逻辑型, 公开, 从指定窗口句柄处获取自绘对象
    .参数 参数_窗口句柄, 整数型, , 目标自绘窗口句柄
    .局部变量 hr, 整数型
    .局部变量 ppObj, 整数型

    m_self.清除 ()
    m_parent.清除 ()
    m_id ＝ 0
    ppObj ＝ _取对象指针的地址 (m_self)
    hr ＝ AccessibleObjectFromWindow (参数_窗口句柄, #OBJID_WINDOW, { 224, 54, 135, 97, 61, 60, 207, 17, 129, 12, 0, 170, 0, 56, 155, 113 }, ppObj)
    返回 (hr ＝ 0)

.子程序 创建自屏幕坐标, 逻辑型, 公开, 从桌面坐标处获取自绘窗口对象
    .参数 参数_X, 整数型, , 桌面X坐标
    .参数 参数_Y, 整数型, , 桌面Y坐标
    .局部变量 hr, 整数型
    .局部变量 ppObj, 整数型
    .局部变量 item, 变体型

    m_self.清除 ()
    m_parent.清除 ()
    m_id ＝ 0
    ppObj ＝ _取对象指针的地址 (m_self)
    hr ＝ AccessibleObjectFromPoint (参数_X, 参数_Y, ppObj, item)
    .如果真 (hr ≠ 0)
        返回 (假)
    .如果真结束
    
    m_id ＝ item.取数值 ()
    .如果真 (m_id ≠ 0)
        m_parent ＝ m_self
        m_self.清除 ()
    .如果真结束
    返回 (真)

.子程序 创建自对象, , 公开, 内部使用，从指定对象获取自绘窗口对象
    .参数 参数_对象, 对象

    m_self.清除 ()
    m_parent.清除 ()
    m_id ＝ 0
    m_self ＝ 参数_对象

.子程序 创建自ID, , 公开, 内部使用，用从指定ID获取自绘窗口对象
    .参数 参数_ID, 整数型
    .参数 参数_父对象, 对象

    m_self.清除 ()
    m_parent.清除 ()
    m_id ＝ 参数_ID
    m_parent ＝ 参数_父对象
    

.子程序 到变体型, 变体型, 公开
    .局部变量 var, 变体型

    .如果 (m_id ＝ 0)
        var.赋值 (m_self, )
    .否则
        var.赋值 (m_id, )
    .如果结束
    返回 (var)

.子程序 是否空, 逻辑型, 公开
    .如果 (m_id ≠ 0)
        返回 (m_parent.是否为空 ())
    .否则
        返回 (m_self.是否为空 ())
    .如果结束
    

.子程序 取ID, 整数型, 公开
    返回 (m_id)

.子程序 取父元素, 逻辑型, 公开, accParent
    .参数 参数_父元素, 类_Accessible, 参考

    .如果 (m_id ≠ 0)
        参数_父元素.创建自对象 (m_parent)
    .否则
        参数_父元素.创建自对象 (m_self.读对象型属性 (“accParent”, ))
    .如果结束
    返回 (取反 (参数_父元素.是否空 ()))
    

.子程序 取子元素总数, 整数型, 公开, accChildCount
    返回 (m_self.读数值属性 (“accChildCount”, ))
    

.子程序 取子元素, 逻辑型, 公开, accChild
    .参数 参数_索引, 整数型, , 索引从1开始
    .参数 参数_子元素, 类_Accessible, 参考
    .局部变量 obj, 对象
    .局部变量 var, 变体型, , "1"
    .局部变量 cObtained, 整数型
    .局部变量 hr, 整数型

    hr ＝ AccessibleChildren (_取对象指针 (m_self), 参数_索引 － 1, 1, var, cObtained)
    .如果真 (hr ＝ 0 且 cObtained ＝ 1)
        .如果 (var [1].取类型 () ＝ #变体类型.数值型)
            obj ＝ m_self.读对象型属性 (“accChild”, var [1])
        .否则
            obj ＝ var [1].取对象 ()
        .如果结束
        
        .如果 (obj.是否为空 ()) ' 如果为空说明子元素是一个简单元素
            参数_子元素.创建自ID (var [1].取数值 (), m_self)
        .否则
            参数_子元素.创建自对象 (obj)
        .如果结束
        
    .如果真结束
    返回 (取反 (参数_子元素.是否空 ()))

.子程序 枚举子元素, 整数型, 公开, 从指定节点对象枚举下级全部子元素
    .参数 参数_子元素组, 类_Accessible, 参考 数组
    .局部变量 obj, 对象
    .局部变量 var, 变体型, , "0"
    .局部变量 cObtained, 整数型
    .局部变量 hr, 整数型
    .局部变量 i, 整数型
    .局部变量 count, 整数型

    count ＝ 取子元素总数 ()
    .如果真 (count ＝ 0)
        返回 (0)
    .如果真结束
    重定义数组 (var, 假, count)
    hr ＝ AccessibleChildren (_取对象指针 (m_self), 0, count, var, cObtained)
    .如果真 (hr ≠ 0)
        返回 (0)
    .如果真结束
    重定义数组 (参数_子元素组, 假, count)
    .计次循环首 (count, i)
        .如果 (var [i].取类型 () ＝ #变体类型.数值型)
            obj ＝ m_self.读对象型属性 (“accChild”, var [i])
        .否则
            obj ＝ var [i].取对象 ()
        .如果结束
        
        .如果 (obj.是否为空 ()) ' 如果为空说明子元素是一个简单元素
            参数_子元素组 [i].创建自ID (var [i].取数值 (), m_self)
        .否则
            参数_子元素组 [i].创建自对象 (obj)
        .如果结束
        
    .计次循环尾 ()
    返回 (count)
    

.子程序 取名称, 文本型, 公开, accName
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读文本属性 (“accName”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读文本属性 (“accName”, var))
    .如果结束
    

.子程序 置名称, 逻辑型, 公开, accName
    .参数 参数_名称, 文本型
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.写属性 (“accName”, var, 参数_名称))
    .否则
        var.赋值 (0, )
        返回 (m_self.写属性 (“accName”, var, 参数_名称))
    .如果结束
    

.子程序 取值, 文本型, 公开, accValue
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读文本属性 (“accValue”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读文本属性 (“accValue”, var))
    .如果结束
    

.子程序 赋值, 逻辑型, 公开, accValue
    .参数 参数_值, 文本型
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.写属性 (“accValue”, var, 参数_值))
    .否则
        var.赋值 (0, )
        返回 (m_self.写属性 (“accValue”, var, 参数_值))
    .如果结束
    

.子程序 取描述, 文本型, 公开, accDescription
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读文本属性 (“accDescription”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读文本属性 (“accDescription”, var))
    .如果结束
    

.子程序 取角色, 整数型, 公开, accRole 返回 #ROLE_SYSTEM_ 常量之一
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读数值属性 (“accRole”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读数值属性 (“accRole”, var))
    .如果结束
    

.子程序 取角色文本, 文本型, 公开, accRole 
    .局部变量 var, 变体型
    .局部变量 role, 整数型
    .局部变量 lpstr, 文本型
    .局部变量 len, 整数型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        role ＝ m_parent.读数值属性 (“accRole”, var)
    .否则
        var.赋值 (0, )
        role ＝ m_self.读数值属性 (“accRole”, var)
    .如果结束
    len ＝ GetRoleText_int (role, 0, 0) ＋ 1
    lpstr ＝ 取空白文本 (len)
    GetRoleText (role, lpstr, len)
    返回 (lpstr)

.子程序 取状态, 整数型, 公开, accState 返回 #STATE_SYSTEM_ 常量组合
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读数值属性 (“accState”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读数值属性 (“accState”, var))
    .如果结束
    

.子程序 取状态文本, 文本型, 公开, GetStateText 
    .局部变量 var, 变体型
    .局部变量 state, 整数型
    .局部变量 i, 整数型
    .局部变量 str, 文本型
    .局部变量 lpstr, 文本型
    .局部变量 len, 整数型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        state ＝ m_parent.读数值属性 (“accState”, var)
    .否则
        var.赋值 (0, )
        state ＝ m_self.读数值属性 (“accState”, var)
    .如果结束
    
    .如果真 (state ＝ #STATE_SYSTEM_NORMAL)
        len ＝ GetStateText_int (state, 0, 0) ＋ 1
        lpstr ＝ 取空白文本 (len)
        GetStateText (state, lpstr, len)
        返回 (lpstr)
    .如果真结束
    
    i ＝ #STATE_SYSTEM_UNAVAILABLE
    .循环判断首 ()
        .如果真 (位与 (state, i) ≠ 0)
            len ＝ GetStateText_int (i, 0, 0) ＋ 1
            lpstr ＝ 取空白文本 (len)
            GetStateText (i, lpstr, len)
            str ＝ str ＋ lpstr ＋ “,”
        .如果真结束
        i ＝ 左移 (i, 1)
    .循环判断尾 (i ≤ #STATE_SYSTEM_ALERT_HIGH)
    返回 (str)
    

.子程序 取帮助, 文本型, 公开, accHelp
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读文本属性 (“accHelp”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读文本属性 (“accHelp”, var))
    .如果结束
    

.子程序 取帮助主题, 整数型, 公开, accHelpTopic
    .局部变量 var, 变体型
    .局部变量 HelpFile, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读数值属性 (“accHelpTopic”, HelpFile, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读数值属性 (“accHelpTopic”, HelpFile, var))
    .如果结束
    

.子程序 取快捷键, 文本型, 公开, accKeyboardShortcut
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读文本属性 (“accKeyboardShortcut”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读文本属性 (“accKeyboardShortcut”, var))
    .如果结束
    

.子程序 取默认动作, 文本型, 公开, accDefaultAction
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        返回 (m_parent.读文本属性 (“accDefaultAction”, var))
    .否则
        var.赋值 (0, )
        返回 (m_self.读文本属性 (“accDefaultAction”, var))
    .如果结束
    

.子程序 取焦点元素, 逻辑型, 公开, accFocus
    .参数 参数_焦点元素, 类_Accessible, 参考
    .局部变量 var, 变体型

    .如果 (m_id ＝ 0)
        var ＝ m_self.读属性 (“accFocus”, )
        .如果真 (var.取类型 () ＝ #变体类型.空)
            返回 (假)
        .如果真结束
        .如果 (var.取类型 () ＝ #变体类型.数值型)
            参数_焦点元素.创建自ID (var.取数值 (), m_self)
        .否则
            参数_焦点元素.创建自对象 (var.取对象 ())
        .如果结束
        
    .否则
        var ＝ m_parent.读属性 (“accFocus”, )
        .如果真 (var.取类型 () ＝ #变体类型.空)
            返回 (假)
        .如果真结束
        .如果 (var.取类型 () ＝ #变体类型.数值型)
            参数_焦点元素.创建自ID (var.取数值 (), m_parent)
        .否则
            参数_焦点元素.创建自对象 (var.取对象 ())
        .如果结束
        
    .如果结束
    
    返回 (取反 (参数_焦点元素.是否空 ()))
    

.子程序 取选择元素, 逻辑型, 公开, accSelection
    .参数 参数_选择元素, 类_Accessible, 参考
    .局部变量 var, 变体型

    .如果 (m_id ＝ 0)
        var ＝ m_self.读属性 (“accSelection”, )
        .如果真 (var.取类型 () ＝ #变体类型.空)
            返回 (假)
        .如果真结束
        .如果 (var.取类型 () ＝ #变体类型.数值型)
            参数_选择元素.创建自ID (var.取数值 (), m_self)
        .否则
            参数_选择元素.创建自对象 (var.取对象 ())
        .如果结束
        
    .否则
        var ＝ m_parent.读属性 (“accSelection”, )
        .如果真 (var.取类型 () ＝ #变体类型.空)
            返回 (假)
        .如果真结束
        .如果 (var.取类型 () ＝ #变体类型.数值型)
            参数_选择元素.创建自ID (var.取数值 (), m_parent)
        .否则
            参数_选择元素.创建自对象 (var.取对象 ())
        .如果结束
        
    .如果结束
    
    返回 (取反 (参数_选择元素.是否空 ()))
    

.子程序 选择元素, , 公开, accSelect
    .参数 参数_标志, 整数型, , #SELFLAG_ 常量组合
    .参数 参数_被选择元素, 类_Accessible, 可空, 如果为空则表示自身被选择
    .局部变量 var, 变体型

    .如果 (是否为空 (参数_被选择元素))
        var.赋值 (0, )
    .否则
        var ＝ 参数_被选择元素.到变体型 ()
    .如果结束
    
    .如果 (m_id ＝ 0)
        m_self.方法 (“accSelect”, 参数_标志, var)
    .否则
        m_parent.方法 (“accSelect”, 参数_标志, var)
    .如果结束
    

.子程序 取位置, , 公开, accLocation
    .参数 参数_左边, 整数型, 参考
    .参数 参数_顶边, 整数型, 参考
    .参数 参数_宽度, 整数型, 参考
    .参数 参数_高度, 整数型, 参考
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        m_parent.方法 (“accLocation”, 参数_左边, 参数_顶边, 参数_宽度, 参数_高度, var)
    .否则
        var.赋值 (0, )
        m_self.方法 (“accLocation”, 参数_左边, 参数_顶边, 参数_宽度, 参数_高度, var)
    .如果结束
    

.子程序 导航, 逻辑型, 公开, accNavigate
    .参数 参数_标志, 整数型, , #NAVDIR_ 常量之一
    .参数 参数_开始元素, 类_Accessible, 可空
    .参数 参数_返回元素, 类_Accessible, 参考
    .局部变量 varStart, 变体型
    .局部变量 varEnd, 变体型

    varStart ＝ 参数_开始元素.到变体型 ()
    .如果 (m_id ≠ 0)
        varEnd ＝ m_parent.通用方法 (“accNavigate”, 参数_标志, varStart)
        .如果 (varEnd.取类型 () ＝ #变体类型.数值型)
            参数_返回元素.创建自ID (varEnd.取数值 (), m_parent)
        .否则
            参数_返回元素.创建自对象 (varEnd.取对象 ())
        .如果结束
        
    .否则
        varEnd ＝ m_self.通用方法 (“accNavigate”, 参数_标志, varStart)
        .如果 (varEnd.取类型 () ＝ #变体类型.数值型)
            参数_返回元素.创建自ID (varEnd.取数值 (), m_self)
        .否则
            参数_返回元素.创建自对象 (varEnd.取对象 ())
        .如果结束
        
    .如果结束
    返回 (取反 (参数_返回元素.是否空 ()))
    

.子程序 命中测试, 逻辑型, 公开, accHitTest
    .参数 参数_左边, 整数型
    .参数 参数_顶边, 整数型
    .参数 参数_元素, 类_Accessible, 参考
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var ＝ m_parent.通用方法 (“accHitTest”, 参数_左边, 参数_顶边)
    .否则
        var ＝ m_self.通用方法 (“accHitTest”, 参数_左边, 参数_顶边)
    .如果结束
    
    .如果真 (var.取类型 () ＝ #变体类型.空)
        返回 (假)
    .如果真结束
    
    .如果真 (var.取类型 () ＝ #变体类型.数值型)
        .如果 (var.取数值 () ＝ 0)
            参数_元素.创建自对象 (m_self)
        .否则
            参数_元素.创建自ID (var.取数值 (), m_self)
        .如果结束
        返回 (真)
    .如果真结束
    
    .如果真 (var.取类型 () ＝ #变体类型.对象型)
        参数_元素.创建自对象 (var.取对象 ())
        返回 (真)
    .如果真结束
    返回 (假)
    
    

.子程序 执行默认动作, , 公开, accDoDefaultAction
    .局部变量 var, 变体型

    .如果 (m_id ≠ 0)
        var.赋值 (m_id, )
        m_parent.方法 (“accDoDefaultAction”, var)
    .否则
        var.赋值 (0, )
        m_self.方法 (“accDoDefaultAction”, var)
    .如果结束
    

.子程序 取窗口句柄, 整数型, 公开
    .局部变量 pobj, 整数型
    .局部变量 hwnd, 整数型

    .如果 (m_id ≠ 0)
        pobj ＝ _取对象指针 (m_parent)
    .否则
        pobj ＝ _取对象指针 (m_self)
    .如果结束
    WindowFromAccessibleObject (pobj, hwnd)
    返回 (hwnd)
    

.子程序 获取对象, , 公开
    .参数 参_对象m_parent, 对象, 参考
    .参数 参_对象m_self, 对象, 参考

    参_对象m_parent ＝ m_parent
    参_对象m_self ＝ m_self
    

