﻿.版本 2

.程序集 类_位图处理, , 公开, 作: 为你芯冻
.子程序 _初始化, , , 当基于本类的对象被创建后，此方法会被自动调用
    

.子程序 _销毁, , , 当基于本类的对象被销毁前，此方法会被自动调用
    

.子程序 取位图颜色, 整数型, 公开
    .参数 位图, 字节集
    .参数 横坐标, 整数型, 可空, 范围为 0~图像宽度 - 1；留空为0。表示图像最左侧。
    .参数 纵坐标, 整数型, 可空, 范围为 0~图像高度 - 1；留空为0。表示图像最顶边。
    .局部变量 信息大小, 整数型, , , BITMAPINFOHEADER -> biSize        一般为 40，但从实际数据获取较保险
    .局部变量 图像宽度, 整数型, , , BITMAPINFOHEADER -> biWidth
    .局部变量 图像高度, 整数型, , , BITMAPINFOHEADER -> biHeight       > 0 从下至上逆向存储； < 0 从上至下正向存储
    .局部变量 图像位数, 短整数型, , , BITMAPINFOHEADER -> biBitCount
    .局部变量 图像压缩, 整数型, , , BITMAPINFOHEADER -> biCompression  1/2-压缩图像; 4/5-JPG/PNG  0-未压缩; 3-未压缩且调色板为3色掩码(位域方式)
    .局部变量 扫描行, 整数型, , , 固定公式计算
    .局部变量 坐标偏移, 整数型, , , 固定公式计算，返回相对于图像数据阵列起始位置的偏移量
    .局部变量 数据偏移, 整数型, , , 文件头 + 信息头 + 调色板(如果有) + 坐标偏移，即指定坐标颜色数据(或其调色板索引)相对于图形数据的偏移量
    .局部变量 色板大小, 整数型, , , 调色板大小，如果有。
    .局部变量 RGB, 字节集
    .局部变量 字节位移, 整数型, , , 用于 图像位数 = 1 或 4 时取指定位
    .局部变量 定点数据, 整数型, , , 颜色数据所在位置、颜色(或调色板)索引等

    .如果真 (取字节集左边 (位图, 2) ≠ { 66, 77 }) ' { 66, 77 } BM 标志
        返回 (-1)
    .如果真结束
    信息大小 ＝ 取字节集数据 (取字节集中间 (位图, 15, 4), #整数型, )
    图像宽度 ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), #整数型, )
    图像高度 ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), #整数型, )
    图像位数 ＝ 取字节集数据 (取字节集中间 (位图, 29, 2), #短整数型, )
    图像压缩 ＝ 取字节集数据 (取字节集中间 (位图, 31, 4), #整数型, )
    .如果真 (图像压缩 ≠ 0 且 图像压缩 ≠ 3)
        返回 (-1)
    .如果真结束
    .如果真 (横坐标 ＜ 0 或 纵坐标 ＜ 0 或 横坐标 ＞ 图像宽度 － 1 或 纵坐标 ＞ 图像高度 － 1)
        返回 (-1)
    .如果真结束
    .判断开始 (图像位数 ＝ 16 或 图像位数 ＝ 32)
        色板大小 ＝ 选择 (图像压缩 ＝ 0, 0, 12)
    .判断 (图像位数 ＝ 24)
        色板大小 ＝ 0
    .判断 (图像位数 ＝ 1 或 图像位数 ＝ 4 或 图像位数 ＝ 8) ' 有调色板
        色板大小 ＝ 左移 (取字节集数据 (取字节集中间 (位图, 47, 4), #整数型, ), 2) ' 实际使用色彩数 * 4
        .如果真 (色板大小 ＝ 0)
            色板大小 ＝ 左移 (4, 图像位数)
        .如果真结束
        
    .默认
        返回 (-1)
    .判断结束
    扫描行 ＝ 左移 (右移 (图像宽度 × 图像位数 ＋ 31, 5), 2) ' 备用写法：右移 (位与 (图像宽度 × 图像位数 ＋ 31, 位取反 (31)), 3)
    坐标偏移 ＝ (右移 (图像高度, 31) × (图像高度 ＋ 1) ＋ 图像高度 － 位或 (右移 (图像高度, 31), 1) × (纵坐标 ＋ 1)) × 扫描行 ＋ 右移 (横坐标 × 图像位数, 3) ＋ 1
    数据偏移 ＝ 14 ＋ 信息大小 ＋ 色板大小 ＋ 坐标偏移
    RGB ＝ { 0, 0, 0, 0 }
    .判断开始 (图像位数 ＝ 32) ' 3 字节表示 1个像素颜色
        RGB ＝ 取字节集中间 (位图, 数据偏移, 4)
    .判断 (图像位数 ＝ 24)
        RGB [1] ＝ 位图 [数据偏移 ＋ 2]
        RGB [2] ＝ 位图 [数据偏移 ＋ 1]
        RGB [3] ＝ 位图 [数据偏移]
    .判断 (图像位数 ＝ 1 或 图像位数 ＝ 4 或 图像位数 ＝ 8) ' 有调色板，1 字节表示 8/2/1 个像素颜色的调色板索引
        定点数据 ＝ 位图 [数据偏移] ' 色彩索引字节
        字节位移 ＝ 位与 (横坐标 ＋ 1, 8 ÷ 图像位数 － 1)
        .如果真 (字节位移 ＞ 0)
            定点数据 ＝ 右移 (定点数据, 8 － 图像位数 × 字节位移)
        .如果真结束
        定点数据 ＝ 位与 (定点数据, 左移 (1, 图像位数) － 1)
        定点数据 ＝ 14 ＋ 信息大小 ＋ 左移 (定点数据, 2) ＋ 1 ' 指定色调色板偏移基址
        RGB [1] ＝ 位图 [定点数据 ＋ 2]
        RGB [2] ＝ 位图 [定点数据 ＋ 1]
        RGB [3] ＝ 位图 [定点数据]
    .默认
        
    .判断结束
    返回 (取字节集数据 (RGB, #整数型, ))

.子程序 置位图颜色, 字节集, 公开
    .参数 位图, 字节集
    .参数 横坐标, 整数型, 可空, 范围为 0~图像宽度 - 1；留空为0。表示图像最左侧。
    .参数 纵坐标, 整数型, 可空, 范围为 0~图像高度 - 1；留空为0。表示图像最顶边。
    .参数 颜色值, 整数型
    .局部变量 信息大小, 整数型, , , BITMAPINFOHEADER -> biSize        一般为 40，但从实际数据获取较保险
    .局部变量 图像宽度, 整数型, , , BITMAPINFOHEADER -> biWidth
    .局部变量 图像高度, 整数型, , , BITMAPINFOHEADER -> biHeight       > 0 从下至上逆向存储； < 0 从上至下正向存储
    .局部变量 图像位数, 短整数型, , , BITMAPINFOHEADER -> biBitCount
    .局部变量 图像压缩, 整数型, , , BITMAPINFOHEADER -> biCompression  1/2-压缩图像; 4/5-JPG/PNG  0-未压缩; 3-未压缩且调色板为3色掩码(位域方式)
    .局部变量 扫描行, 整数型, , , 固定公式计算
    .局部变量 坐标偏移, 整数型, , , 固定公式计算，返回相对于图像数据阵列起始位置的偏移量
    .局部变量 数据偏移, 整数型, , , 文件头 + 信息头 + 调色板(如果有) + 坐标偏移，即指定坐标颜色数据(或其调色板索引)相对于图形数据的偏移量
    .局部变量 色板大小, 整数型, , , 调色板大小，如果有。
    .局部变量 RGB, 字节集

    .如果真 (取字节集左边 (位图, 2) ≠ { 66, 77 }) ' { 66, 77 } BM 标志
        返回 ({ })
    .如果真结束
    信息大小 ＝ 取字节集数据 (取字节集中间 (位图, 15, 4), #整数型, )
    图像宽度 ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), #整数型, )
    图像高度 ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), #整数型, )
    图像位数 ＝ 取字节集数据 (取字节集中间 (位图, 29, 2), #短整数型, )
    图像压缩 ＝ 取字节集数据 (取字节集中间 (位图, 31, 4), #整数型, )
    .如果真 (图像压缩 ≠ 0 且 图像压缩 ≠ 3)
        返回 (位图)
    .如果真结束
    .如果真 (横坐标 ＜ 0 或 纵坐标 ＜ 0 或 横坐标 ＞ 图像宽度 － 1 或 纵坐标 ＞ 图像高度 － 1)
        返回 (位图)
    .如果真结束
    .判断开始 (图像位数 ＝ 16 或 图像位数 ＝ 32)
        色板大小 ＝ 选择 (图像压缩 ＝ 0, 0, 12)
    .判断 (图像位数 ＝ 24)
        色板大小 ＝ 0
    .默认
        返回 (位图)
    .判断结束
    扫描行 ＝ 左移 (右移 (图像宽度 × 图像位数 ＋ 31, 5), 2) ' 备用写法：右移 (位与 (图像宽度 × 图像位数 ＋ 31, 位取反 (31)), 3)
    坐标偏移 ＝ (右移 (图像高度, 31) × (图像高度 ＋ 1) ＋ 图像高度 － 位或 (右移 (图像高度, 31), 1) × (纵坐标 ＋ 1)) × 扫描行 ＋ 右移 (横坐标 × 图像位数, 3) ＋ 1
    数据偏移 ＝ 14 ＋ 信息大小 ＋ 色板大小 ＋ 坐标偏移
    RGB ＝ 到字节集 (颜色值)
    .判断开始 (图像位数 ＝ 32) ' 3 字节表示 1个像素颜色
        位图 ＝ 字节集替换 (位图, 数据偏移, 4, RGB)
    .判断 (图像位数 ＝ 24)
        位图 [数据偏移 ＋ 2] ＝ RGB [1]
        位图 [数据偏移 ＋ 1] ＝ RGB [2]
        位图 [数据偏移] ＝ RGB [3]
    .默认
        
    .判断结束
    返回 (位图)

.子程序 取位图颜色表, 整数型, 公开
    .参数 位图, 字节集
    .参数 颜色表, 整数型, 参考 数组
    .局部变量 信息大小, 整数型, , , BITMAPINFOHEADER -> biSize        一般为 40，但从实际数据获取较保险
    .局部变量 图像宽度, 整数型, , , BITMAPINFOHEADER -> biWidth
    .局部变量 图像高度, 整数型, , , BITMAPINFOHEADER -> biHeight       > 0 从下至上逆向存储； < 0 从上至下正向存储
    .局部变量 图像位数, 短整数型, , , BITMAPINFOHEADER -> biBitCount
    .局部变量 图像压缩, 整数型, , , BITMAPINFOHEADER -> biCompression  1/2-压缩图像; 4/5-JPG/PNG  0-未压缩; 3-未压缩且调色板为3色掩码(位域方式)
    .局部变量 扫描行, 整数型, , , 固定公式计算
    .局部变量 坐标偏移, 整数型, , , 固定公式计算，返回相对于图像数据阵列起始位置的偏移量
    .局部变量 数据偏移, 整数型, , , 文件头 + 信息头 + 调色板(如果有) + 坐标偏移，即指定坐标颜色数据(或其调色板索引)相对于图形数据的偏移量
    .局部变量 色板大小, 整数型, , , 调色板大小，如果有。
    .局部变量 RGB, 字节集
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 定点数据, 整数型
    .局部变量 字节位移, 整数型

    .如果真 (取字节集左边 (位图, 2) ≠ { 66, 77 }) ' { 66, 77 } BM 标志
        返回 (0)
    .如果真结束
    信息大小 ＝ 取字节集数据 (取字节集中间 (位图, 15, 4), #整数型, )
    图像宽度 ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), #整数型, )
    图像高度 ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), #整数型, )
    图像位数 ＝ 取字节集数据 (取字节集中间 (位图, 29, 2), #短整数型, )
    图像压缩 ＝ 取字节集数据 (取字节集中间 (位图, 31, 4), #整数型, )
    .如果真 (图像压缩 ≠ 0 且 图像压缩 ≠ 3)
        返回 (0)
    .如果真结束
    .判断开始 (图像位数 ＝ 16 或 图像位数 ＝ 32)
        色板大小 ＝ 选择 (图像压缩 ＝ 0, 0, 12)
    .判断 (图像位数 ＝ 24)
        色板大小 ＝ 0
    .判断 (图像位数 ＝ 1 或 图像位数 ＝ 4 或 图像位数 ＝ 8) ' 有调色板
        色板大小 ＝ 左移 (取字节集数据 (取字节集中间 (位图, 47, 4), #整数型, ), 2) ' 实际使用色彩数 * 4
        .如果真 (色板大小 ＝ 0)
            色板大小 ＝ 左移 (4, 图像位数)
        .如果真结束
        
    .默认
        返回 (0)
    .判断结束
    扫描行 ＝ 左移 (右移 (图像宽度 × 图像位数 ＋ 31, 5), 2) ' 备用写法：右移 (位与 (图像宽度 × 图像位数 ＋ 31, 位取反 (31)), 3)
    重定义数组 (颜色表, 假, 图像宽度, 图像高度)
    .计次循环首 (图像宽度, x)
        .计次循环首 (图像高度, y)
            坐标偏移 ＝ (右移 (图像高度, 31) × (图像高度 ＋ 1) ＋ 图像高度 － 位或 (右移 (图像高度, 31), 1) × y) × 扫描行 ＋ 右移 ((x － 1) × 图像位数, 3) ＋ 1
            数据偏移 ＝ 14 ＋ 信息大小 ＋ 色板大小 ＋ 坐标偏移
            RGB ＝ { 0, 0, 0, 0 }
            .判断开始 (图像位数 ＝ 32) ' 3 字节表示 1个像素颜色
                RGB ＝ 取字节集中间 (位图, 数据偏移, 4)
            .判断 (图像位数 ＝ 24)
                RGB [1] ＝ 位图 [数据偏移 ＋ 2]
                RGB [2] ＝ 位图 [数据偏移 ＋ 1]
                RGB [3] ＝ 位图 [数据偏移]
            .判断 (图像位数 ＝ 1 或 图像位数 ＝ 4 或 图像位数 ＝ 8) ' 有调色板，1 字节表示 8/2/1 个像素颜色的调色板索引
                定点数据 ＝ 位图 [数据偏移] ' 色彩索引字节
                字节位移 ＝ 位与 (x, 8 ÷ 图像位数 － 1)
                .如果真 (字节位移 ＞ 0)
                    定点数据 ＝ 右移 (定点数据, 8 － 图像位数 × 字节位移)
                .如果真结束
                定点数据 ＝ 位与 (定点数据, 左移 (1, 图像位数) － 1)
                定点数据 ＝ 14 ＋ 信息大小 ＋ 左移 (定点数据, 2) ＋ 1 ' 指定色调色板偏移基址
                RGB [1] ＝ 位图 [定点数据 ＋ 2]
                RGB [2] ＝ 位图 [定点数据 ＋ 1]
                RGB [3] ＝ 位图 [定点数据]
            .默认
                
            .判断结束
            颜色表 [x] [y] ＝ 取字节集数据 (RGB, 3, )
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    返回 (图像位数)

.子程序 颜色表构建位图, 字节集, 公开, 如果颜色表是24位颜色表的话要构建8位位图 必须先把24位颜色表改成8位颜色表
    .参数 颜色表, 整数型, 数组, 输进一个二维数组,分别表示宽和高,数组值为颜色值
    .参数 位数, 整数型, 可空, 默认为24
    .局部变量 w, 整数型
    .局部变量 w1, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 i, 整数型
    .局部变量 n, 整数型
    .局部变量 位图, 字节集
    .局部变量 调色板, 整数型, , "0"

    .如果真 (取数组成员数 (颜色表) ＝ 0)
        返回 ({ })
    .如果真结束
    位数 ＝ 选择 (是否为空 (位数), 24, 位数)
    w ＝ 取字节集数据 (指针到字节集 (lstrcpynA_整数数组 (颜色表, 颜色表, 0) － 8, 4), 3, )
    h ＝ 取字节集数据 (指针到字节集 (lstrcpynA_整数数组 (颜色表, 颜色表, 0) － 4, 4), 3, )
    .判断开始 (位数 ＝ 24)
        .计次循环首 (h, y)
            .计次循环首 (w, x)
                位图 ＝ 位图 ＋ 字节集_反转 (取字节集左边 (到字节集 (颜色表 [x] [h ＋ 1 － y]), 3))
            .计次循环尾 ()
            .如果真 (取字节集长度 (位图) ÷ 4 ≠ 到整数 (取字节集长度 (位图) ÷ 4))
                位图 ＝ 位图 ＋ 取空白字节集 ((1 － 取字节集长度 (位图) ÷ 4 ＋ 到整数 (取字节集长度 (位图) ÷ 4)) × 4)
            .如果真结束
            处理事件 ()
        .计次循环尾 ()
        位图 ＝ { 66, 77 } ＋ 到字节集 (到整数 (58 ＋ 取字节集长度 (位图))) ＋ { 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0 } ＋ 到字节集 (w) ＋ 到字节集 (h) ＋ { 1, 0, 24, 0, 0, 0, 0, 0 } ＋ 到字节集 (取字节集长度 (位图)) ＋ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ＋ 位图
    .判断 (位数 ＝ 1)
        调色板 ＝ { #黑色, #白色 }
        w1 ＝ 到整数 (w ÷ 8)
        .如果真 (w1 ≠ w ÷ 8)
            w1 ＝ w1 ＋ 1
        .如果真结束
        .计次循环首 (h, y)
            .计次循环首 (w1, x)
                位图 ＝ 位图 ＋ { 0 }
                .计次循环首 (8, i)
                    .如果真 ((x － 1) × 8 ＋ i ≤ w)
                        .如果真 (颜色表 [(x － 1) × 8 ＋ i] [h ＋ 1 － y] ＝ #白色)
                            位图 [取字节集长度 (位图)] ＝ 位图 [取字节集长度 (位图)] ＋ 求次方 (2, 8 － i)
                        .如果真结束
                        
                    .如果真结束
                    
                .计次循环尾 ()
            .计次循环尾 ()
            .如果真 (取字节集长度 (位图) ÷ 4 ≠ 到整数 (取字节集长度 (位图) ÷ 4))
                位图 ＝ 位图 ＋ 取空白字节集 ((1 － 取字节集长度 (位图) ÷ 4 ＋ 到整数 (取字节集长度 (位图) ÷ 4)) × 4)
            .如果真结束
            处理事件 ()
        .计次循环尾 ()
        位图 ＝ { 66, 77 } ＋ 到字节集 (到整数 (66 ＋ 取字节集长度 (位图))) ＋ { 0, 0, 0, 0, 62, 0, 0, 0, 40, 0, 0, 0 } ＋ 到字节集 (w) ＋ 到字节集 (h) ＋ { 1, 0, 1, 0, 0, 0, 0, 0 } ＋ 到字节集 (取字节集长度 (位图)) ＋ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ＋ 指针到字节集 (lstrcpynA_整数数组 (调色板, 调色板, 0), 取数组成员数 (调色板) × 4) ＋ 位图
    .判断 (位数 ＝ 4)
        调色板 ＝ { 0, 8388608, 32768, 8421376, 128, 8388736, 32896, 8421504, 12632256, 16711680, 65280, 16776960, 255, 16711935, 65535, 16777215 }
        w1 ＝ 到整数 (w ÷ 2)
        .如果真 (w1 ≠ w ÷ 2)
            w1 ＝ w1 ＋ 1
        .如果真结束
        .计次循环首 (h, y)
            .计次循环首 (w1, x)
                位图 ＝ 位图 ＋ { 0 }
                .计次循环首 (2, i)
                    .如果真 ((x － 1) × 2 ＋ i ≤ w)
                        .计次循环首 (取数组成员数 (调色板), n)
                            .如果真 (调色板 [n] ＝ 颜色表 [(x － 1) × 2 ＋ i] [h ＋ 1 － y])
                                跳出循环 ()
                            .如果真结束
                            
                        .计次循环尾 ()
                        n ＝ 选择 (n ＝ 取数组成员数 (调色板) ＋ 1, n － 2, n － 1)
                        位图 [取字节集长度 (位图)] ＝ 位图 [取字节集长度 (位图)] ＋ n × 求次方 (16, 2 － i)
                        
                    .如果真结束
                    
                .计次循环尾 ()
            .计次循环尾 ()
            .如果真 (取字节集长度 (位图) ÷ 4 ≠ 到整数 (取字节集长度 (位图) ÷ 4))
                位图 ＝ 位图 ＋ 取空白字节集 ((1 － 取字节集长度 (位图) ÷ 4 ＋ 到整数 (取字节集长度 (位图) ÷ 4)) × 4)
            .如果真结束
            处理事件 ()
        .计次循环尾 ()
        位图 ＝ { 66, 77 } ＋ 到字节集 (到整数 (122 ＋ 取字节集长度 (位图))) ＋ { 0, 0, 0, 0, 118, 0, 0, 0, 40, 0, 0, 0 } ＋ 到字节集 (w) ＋ 到字节集 (h) ＋ { 1, 0, 4, 0, 0, 0, 0, 0 } ＋ 到字节集 (取字节集长度 (位图)) ＋ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ＋ 指针到字节集 (lstrcpynA_整数数组 (调色板, 调色板, 0), 取数组成员数 (调色板) × 4) ＋ 位图
    .判断 (位数 ＝ 8)
        调色板 ＝ { 0, 65793, 131586, 197379, 263172, 328965, 394758, 460551, 526344, 592137, 657930, 723723, 789516, 855309, 921102, 986895, 1052688, 1118481, 1184274, 1250067, 1315860, 1381653, 1447446, 1513239, 1579032, 1644825, 1710618, 1776411, 1842204, 1907997, 1973790, 2039583, 2105376, 2171169, 2236962, 2302755, 2368548, 2434341, 2500134, 2565927, 2631720, 2697513, 2763306, 2829099, 2894892, 2960685, 3026478, 3092271, 3158064, 3223857, 3289650, 3355443, 3421236, 3487029, 3552822, 3618615, 3684408, 3750201, 3815994, 3881787, 3947580, 4013373, 4079166, 4144959, 4210752, 4276545, 4342338, 4408131, 4473924, 4539717, 4605510, 4671303, 4737096, 4802889, 4868682, 4934475, 5000268, 5066061, 5131854, 5197647, 5263440, 5329233, 5395026, 5460819, 5526612, 5592405, 5658198, 5723991, 5789784, 5855577, 5921370, 5987163, 6052956, 6118749, 6184542, 6250335, 6316128, 6381921, 6447714, 6513507, 6579300, 6645093, 6710886, 6776679, 6842472, 6908265, 6974058, 7039851, 7105644, 7171437, 7237230, 7303023, 7368816, 7434609, 7500402, 7566195, 7631988, 7697781, 7763574, 7829367, 7895160, 7960953, 8026746, 8092539, 8158332, 8224125, 8289918, 8355711, 8421504, 8487297, 8553090, 8618883, 8684676, 8750469, 8816262, 8882055, 8947848, 9013641, 9079434, 9145227, 9211020, 9276813, 9342606, 9408399, 9474192, 9539985, 9605778, 9671571, 9737364, 9803157, 9868950, 9934743, 10000536, 10066329, 10132122, 10197915, 10263708, 10329501, 10395294, 10461087, 10526880, 10592673, 10658466, 10724259, 10790052, 10855845, 10921638, 10987431, 11053224, 11119017, 11184810, 11250603, 11316396, 11382189, 11447982, 11513775, 11579568, 11645361, 11711154, 11776947, 11842740, 11908533, 11974326, 12040119, 12105912, 12171705, 12237498, 12303291, 12369084, 12434877, 12500670, 12566463, 12632256, 12698049, 12763842, 12829635, 12895428, 12961221, 13027014, 13092807, 13158600, 13224393, 13290186, 13355979, 13421772, 13487565, 13553358, 13619151, 13684944, 13750737, 13816530, 13882323, 13948116, 14013909, 14079702, 14145495, 14211288, 14277081, 14342874, 14408667, 14474460, 14540253, 14606046, 14671839, 14737632, 14803425, 14869218, 14935011, 15000804, 15066597, 15132390, 15198183, 15263976, 15329769, 15395562, 15461355, 15527148, 15592941, 15658734, 15724527, 15790320, 15856113, 15921906, 15987699, 16053492, 16119285, 16185078, 16250871, 16316664, 16382457, 16448250, 16514043, 16579836, 16645629, 16711422, 16777215 }
        .计次循环首 (h, y)
            .计次循环首 (w, x)
                位图 ＝ 位图 ＋ { 0 }
                .计次循环首 (取数组成员数 (调色板), n)
                    .如果真 (调色板 [n] ＝ 颜色表 [x] [h ＋ 1 － y])
                        跳出循环 ()
                    .如果真结束
                    
                .计次循环尾 ()
                n ＝ 选择 (n ＝ 取数组成员数 (调色板) ＋ 1, n － 2, n － 1)
                位图 [取字节集长度 (位图)] ＝ n
            .计次循环尾 ()
            .如果真 (取字节集长度 (位图) ÷ 4 ≠ 到整数 (取字节集长度 (位图) ÷ 4))
                位图 ＝ 位图 ＋ 取空白字节集 ((1 － 取字节集长度 (位图) ÷ 4 ＋ 到整数 (取字节集长度 (位图) ÷ 4)) × 4)
            .如果真结束
            处理事件 ()
        .计次循环尾 ()
        位图 ＝ { 66, 77 } ＋ 到字节集 (到整数 (2102 ＋ 取字节集长度 (位图))) ＋ { 0, 0, 0, 0, 54, 4, 0, 0, 40, 0, 0, 0 } ＋ 到字节集 (w) ＋ 到字节集 (h) ＋ { 1, 0, 8, 0, 0, 0, 0, 0 } ＋ 到字节集 (取字节集长度 (位图)) ＋ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ＋ 指针到字节集 (lstrcpynA_整数数组 (调色板, 调色板, 0), 取数组成员数 (调色板) × 4) ＋ 位图
    .判断 (位数 ＝ 32)
        .计次循环首 (h, y)
            .计次循环首 (w, x)
                位图 ＝ 位图 ＋ 字节集_反转 (取字节集左边 (到字节集 (颜色表 [x] [h ＋ 1 － y]), 3)) ＋ { 0 }
            .计次循环尾 ()
            处理事件 ()
        .计次循环尾 ()
        位图 ＝ { 66, 77 } ＋ 到字节集 (到整数 (58 ＋ 取字节集长度 (位图))) ＋ { 0, 0, 0, 0, 54, 0, 0, 0, 40, 0, 0, 0 } ＋ 到字节集 (w) ＋ 到字节集 (h) ＋ { 1, 0, 32, 0, 0, 0, 0, 0 } ＋ 到字节集 (取字节集长度 (位图)) ＋ { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 } ＋ 位图
    .默认
        
    .判断结束
    返回 (位图)

.子程序 位图去杂点, 字节集, 公开
    .参数 位图, 字节集
    .局部变量 颜色表, 整数型, , "0"
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 i, 整数型
    .局部变量 孤立值, 整数型
    .局部变量 点x, 整数型
    .局部变量 点y, 整数型
    .局部变量 点阵, 整数型, , "8,3"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    写到内存 ({ 1, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 2, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 4, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 8, 0, 0, 0, 1, 0, 0, 0, 255, 255, 255, 255, 16, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 128, 0, 0, 0, 255, 255, 255, 255, 1, 0, 0, 0 }, lstrcpynA_整数数组 (点阵, 点阵, 0), 96)
    取位图颜色表 (位图, 颜色表)
    .计次循环首 (h, y)
        .计次循环首 (w, x)
            点x ＝ -1
            点y ＝ -1
            孤立值 ＝ 点是否孤立 (位图, x － 1, y － 1)
            .如果真 (孤立值 ＝ -1)
                到循环尾 ()
            .如果真结束
            .如果真 (孤立值 ＝ 0)
                颜色表 [x] [y] ＝ #白色
                到循环尾 ()
            .如果真结束
            .计次循环首 (8, i)
                .如果真 (位与 (孤立值, 点阵 [i] [1]) ＝ 点阵 [i] [1])
                    .如果 (点x ＝ -1 且 点y ＝ -1)
                        点x ＝ x ＋ 点阵 [i] [2]
                        点y ＝ y ＋ 点阵 [i] [3]
                    .否则
                        跳出循环 ()
                    .如果结束
                    
                .如果真结束
                
            .计次循环尾 ()
            .如果真 (i ＝ 9)
                i ＝ 点是否孤立 (位图, 点x, 点y)
                .如果真 (i ＝ 孤立值)
                    颜色表 [x] [y] ＝ #白色
                    颜色表 [点x] [点y] ＝ #白色
                .如果真结束
                
            .如果真结束
            
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    位图 ＝ 颜色表构建位图 (颜色表)
    返回 (位图)

.子程序 位图缩放, 字节集, 公开
    .参数 位图, 字节集
    .参数 缩放后宽, 整数型
    .参数 缩放后高, 整数型
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 x0, 双精度小数型
    .局部变量 y0, 双精度小数型
    .局部变量 x1, 整数型
    .局部变量 x2, 整数型
    .局部变量 y1, 整数型
    .局部变量 y2, 整数型
    .局部变量 颜色1, 字节集
    .局部变量 颜色2, 字节集
    .局部变量 缩放颜色r1, 双精度小数型
    .局部变量 缩放颜色g1, 双精度小数型
    .局部变量 缩放颜色b1, 双精度小数型
    .局部变量 缩放颜色r2, 双精度小数型
    .局部变量 缩放颜色g2, 双精度小数型
    .局部变量 缩放颜色b2, 双精度小数型
    .局部变量 dt颜色r, 双精度小数型
    .局部变量 dt颜色g, 双精度小数型
    .局部变量 dt颜色b, 双精度小数型
    .局部变量 缩放宽比, 双精度小数型
    .局部变量 缩放高比, 双精度小数型
    .局部变量 颜色表, 整数型, , "0"
    .局部变量 缩放颜色表, 整数型, , "0"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    取位图颜色表 (位图, 颜色表)
    重定义数组 (缩放颜色表, 假, 缩放后宽, 缩放后高)
    缩放宽比 ＝ w ÷ 缩放后宽
    缩放高比 ＝ h ÷ 缩放后高
    .计次循环首 (缩放后宽, x)
        .计次循环首 (缩放后高, y)
            x0 ＝ x × 缩放宽比
            y0 ＝ y × 缩放高比
            .如果 (到整数 (x0) ≠ x0)
                x1 ＝ 选择 (到整数 (x0) ＜ 1, 1, 到整数 (x0))
                x2 ＝ 到整数 (x0) ＋ 1
            .否则
                x1 ＝ x0
                x2 ＝ x0
            .如果结束
            .如果 (到整数 (y0) ≠ y0)
                y1 ＝ 选择 (到整数 (y0) ＜ 1, 1, 到整数 (y0))
                y2 ＝ 到整数 (y0) ＋ 1
            .否则
                y1 ＝ y0
                y2 ＝ y0
            .如果结束
            颜色1 ＝ 到字节集 (颜色表 [x1] [y1])
            颜色2 ＝ 到字节集 (颜色表 [x1] [y2])
            缩放颜色r1 ＝ 颜色2 [1] － (颜色2 [1] － 颜色1 [1]) × (y2 － y0)
            缩放颜色g1 ＝ 颜色2 [2] － (颜色2 [2] － 颜色1 [2]) × (y2 － y0)
            缩放颜色b1 ＝ 颜色2 [3] － (颜色2 [3] － 颜色1 [3]) × (y2 － y0)
            颜色1 ＝ 到字节集 (颜色表 [x2] [y1])
            颜色2 ＝ 到字节集 (颜色表 [x2] [y2])
            缩放颜色r2 ＝ 颜色2 [1] － (颜色2 [1] － 颜色1 [1]) × (y2 － y0)
            缩放颜色g2 ＝ 颜色2 [2] － (颜色2 [2] － 颜色1 [2]) × (y2 － y0)
            缩放颜色b2 ＝ 颜色2 [3] － (颜色2 [3] － 颜色1 [3]) × (y2 － y0)
            dt颜色r ＝ 缩放颜色r2 － (缩放颜色r2 － 缩放颜色r1) × (x2 － x0)
            dt颜色g ＝ 缩放颜色g2 － (缩放颜色g2 － 缩放颜色g1) × (x2 － x0)
            dt颜色b ＝ 缩放颜色b2 － (缩放颜色b2 － 缩放颜色b1) × (x2 － x0)
            颜色1 [1] ＝ 四舍五入 (dt颜色r, )
            颜色1 [2] ＝ 四舍五入 (dt颜色g, )
            颜色1 [3] ＝ 四舍五入 (dt颜色b, )
            缩放颜色表 [x] [y] ＝ 取字节集数据 (颜色1, 3, )
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    返回 (颜色表构建位图 (缩放颜色表))

.子程序 位图转换位数, 字节集, 公开
    .参数 位图, 字节集
    .参数 位数, 整数型
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 i, 整数型
    .局部变量 rgb1, 字节集
    .局部变量 rgb2, 字节集
    .局部变量 亮度值, 双精度小数型
    .局部变量 颜色值, 整数型
    .局部变量 颜色表, 整数型, , "0"
    .局部变量 调色板, 整数型, , "0"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    取位图颜色表 (位图, 颜色表)
    .如果真 (位数 ＝ 8 或 位数 ＝ 4 或 位数 ＝ 1)
        .计次循环首 (w, x)
            .计次循环首 (h, y)
                rgb1 ＝ 到字节集 (颜色表 [x] [y])
                亮度值 ＝ rgb1 [1] × 0.299 ＋ rgb1 [2] × 0.588 ＋ rgb1 [3] × 0.113
                连续赋值 (四舍五入 (亮度值, ), rgb1 [1], rgb1 [2], rgb1 [3])
                颜色表 [x] [y] ＝ 取字节集数据 (rgb1, 3, )
            .计次循环尾 ()
            处理事件 ()
        .计次循环尾 ()
        .如果真 (位数 ＝ 4 或 位数 ＝ 1)
            .如果 (位数 ＝ 4)
                调色板 ＝ { 0, 8388608, 32768, 8421376, 128, 8388736, 32896, 8421504, 12632256, 16711680, 65280, 16776960, 255, 16711935, 65535, 16777215 }
            .否则
                调色板 ＝ { #黑色, #白色 }
            .如果结束
            .计次循环首 (w, x)
                .计次循环首 (h, y)
                    rgb1 ＝ 到字节集 (颜色表 [x] [y])
                    亮度值 ＝ rgb1 [1]
                    颜色值 ＝ -1
                    .计次循环首 (取数组成员数 (调色板), i)
                        .如果 (颜色值 ＝ -1)
                            颜色值 ＝ 调色板 [i]
                        .否则
                            rgb1 ＝ 到字节集 (颜色值)
                            rgb2 ＝ 到字节集 (调色板 [i])
                            颜色值 ＝ 选择 (取绝对值 (rgb2 [1] × 0.113 ＋ rgb2 [2] × 0.588 ＋ rgb2 [3] × 0.299 － 亮度值) ＜ 取绝对值 (rgb1 [1] × 0.113 ＋ rgb1 [2] × 0.588 ＋ rgb1 [3] × 0.299 － 亮度值), 调色板 [i], 颜色值)
                        .如果结束
                        
                    .计次循环尾 ()
                    颜色表 [x] [y] ＝ 颜色值
                .计次循环尾 ()
                处理事件 ()
            .计次循环尾 ()
        .如果真结束
        
    .如果真结束
    位图 ＝ 颜色表构建位图 (颜色表, 位数)
    返回 (位图)

.子程序 位图二值化, 字节集, 公开
    .参数 位图, 字节集
    .参数 阀值, 整数型
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 rgb, 字节集
    .局部变量 颜色表, 整数型, , "0"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    .如果真 (取位图颜色表 (位图, 颜色表) ＝ 0)
        返回 ({ })
    .如果真结束
    .计次循环首 (w, x)
        .计次循环首 (h, y)
            rgb ＝ 到字节集 (颜色表 [x] [y])
            .如果 (rgb [1] ＜ 阀值 且 rgb [2] ＜ 阀值 且 rgb [3] ＜ 阀值)
                颜色表 [x] [y] ＝ #黑色
            .否则
                颜色表 [x] [y] ＝ #白色
            .如果结束
            
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    位图 ＝ 颜色表构建位图 (颜色表)
    返回 (位图)

.子程序 位图反色, 字节集, 公开
    .参数 位图, 字节集
    .局部变量 颜色表, 整数型, , "0"
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 rgb, 整数型
    .局部变量 bin, 字节集

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    取位图颜色表 (位图, 颜色表)
    .计次循环首 (w, x)
        .计次循环首 (h, y)
            rgb ＝ 颜色表 [x] [y]
            bin ＝ 到字节集 (rgb)
            bin [1] ＝ 255 － bin [1]
            bin [2] ＝ 255 － bin [2]
            bin [3] ＝ 255 － bin [3]
            rgb ＝ 取字节集数据 (bin, 3, )
            颜色表 [x] [y] ＝ rgb
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    位图 ＝ 颜色表构建位图 (颜色表)
    返回 (位图)

.子程序 取位图轮廓图, 字节集, 公开
    .参数 位图, 字节集
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 i, 整数型
    .局部变量 rgb, 整数型
    .局部变量 颜色表, 整数型, , "0"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    取位图颜色表 (位图, 颜色表)
    .计次循环首 (w, x)
        .计次循环首 (h, y)
            rgb ＝ 取位图颜色 (位图, x － 1, y － 1)
            .如果真 (rgb ≠ #白色)
                rgb ＝ 取位图颜色 (位图, x － 2, y － 1)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x － 1, y － 2)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x, y － 1)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x － 1, y)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x － 2, y － 2)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x, y － 2)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x － 2, y)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                rgb ＝ 取位图颜色 (位图, x, y)
                .如果真 (rgb ＝ -1 或 rgb ＝ #白色)
                    到循环尾 ()
                .如果真结束
                颜色表 [x － 1] [y － 1] ＝ #白色
            .如果真结束
            
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    返回 (颜色表构建位图 (颜色表))

.子程序 位图细化, 字节集, 公开
    .参数 位图, 字节集
    .局部变量 表, 字节集
    .局部变量 位图宽度, 整数型
    .局部变量 位图高度, 整数型
    .局部变量 Finished, 逻辑型
    .局部变量 X, 整数型
    .局部变量 Y, 整数型
    .局部变量 w, 整数型
    .局部变量 e, 整数型
    .局部变量 nw, 整数型
    .局部变量 n, 整数型
    .局部变量 ne, 整数型
    .局部变量 sw, 整数型
    .局部变量 s, 整数型
    .局部变量 se, 整数型
    .局部变量 num, 整数型

    表 ＝ { 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0 }
    位图宽度 ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    位图高度 ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    Finished ＝ 假
    .判断循环首 (Finished ＝ 假)
        Finished ＝ 真
        .计次循环首 (位图高度 － 2, Y)
            X ＝ 1
            .判断循环首 (X ＜ 位图宽度 － 2)
                .如果真 (取位图颜色 (位图, X, Y) ≠ #白色)
                    w ＝ 选择 (取位图颜色 (位图, X － 1, Y) ＝ #白色, 1, 0)
                    e ＝ 选择 (取位图颜色 (位图, X ＋ 1, Y) ＝ #白色, 1, 0)
                    .如果真 (w ＝ 1 或 e ＝ 1)
                        nw ＝ 选择 (取位图颜色 (位图, X － 1, Y － 1) ＝ #白色, 1, 0)
                        n ＝ 选择 (取位图颜色 (位图, X, Y － 1) ＝ #白色, 1, 0)
                        ne ＝ 选择 (取位图颜色 (位图, X ＋ 1, Y － 1) ＝ #白色, 1, 0)
                        sw ＝ 选择 (取位图颜色 (位图, X － 1, Y ＋ 1) ＝ #白色, 1, 0)
                        s ＝ 选择 (取位图颜色 (位图, X, Y ＋ 1) ＝ #白色, 1, 0)
                        se ＝ 选择 (取位图颜色 (位图, X ＋ 1, Y ＋ 1) ＝ #白色, 1, 0)
                        num ＝ nw ＋ n × 2 ＋ ne × 4 ＋ w × 8 ＋ e × 16 ＋ sw × 32 ＋ s × 64 ＋ se × 128
                        .如果真 (表 [num ＋ 1] ＝ 1)
                            位图 ＝ 置位图颜色 (位图, X, Y, #白色)
                            Finished ＝ 假
                            X ＝ X ＋ 1
                        .如果真结束
                        
                    .如果真结束
                    
                .如果真结束
                X ＝ X ＋ 1
            .判断循环尾 ()
            处理事件 ()
        .计次循环尾 ()
        .计次循环首 (位图宽度 － 2, X)
            Y ＝ 1
            .判断循环首 (Y ＜ 位图高度 － 2)
                .如果真 (取位图颜色 (位图, X, Y) ≠ #白色)
                    n ＝ 选择 (取位图颜色 (位图, X, Y － 1) ＝ #白色, 1, 0)
                    s ＝ 选择 (取位图颜色 (位图, X, Y ＋ 1) ＝ #白色, 1, 0)
                    .如果真 (n ＝ 1 或 s ＝ 1)
                        nw ＝ 选择 (取位图颜色 (位图, X － 1, Y － 1) ＝ #白色, 1, 0)
                        ne ＝ 选择 (取位图颜色 (位图, X ＋ 1, Y － 1) ＝ #白色, 1, 0)
                        w ＝ 选择 (取位图颜色 (位图, X － 1, Y) ＝ #白色, 1, 0)
                        e ＝ 选择 (取位图颜色 (位图, X ＋ 1, Y) ＝ #白色, 1, 0)
                        sw ＝ 选择 (取位图颜色 (位图, X － 1, Y ＋ 1) ＝ #白色, 1, 0)
                        se ＝ 选择 (取位图颜色 (位图, X ＋ 1, Y ＋ 1) ＝ #白色, 1, 0)
                        num ＝ nw ＋ n × 2 ＋ ne × 4 ＋ w × 8 ＋ e × 16 ＋ sw × 32 ＋ s × 64 ＋ se × 128
                        .如果真 (表 [num ＋ 1] ＝ 1)
                            位图 ＝ 置位图颜色 (位图, X, Y, #白色)
                            Finished ＝ 假
                            Y ＝ Y ＋ 1
                        .如果真结束
                        
                    .如果真结束
                    
                .如果真结束
                Y ＝ Y ＋ 1
            .判断循环尾 ()
            处理事件 ()
        .计次循环尾 ()
    .判断循环尾 ()
    返回 (位图)

.子程序 位图分割1, 整数型, 公开
    .参数 位图, 字节集
    .参数 范围数组, 精易_矩形, 参考 可空 数组
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 i, 整数型
    .局部变量 rgb, 整数型
    .局部变量 范围, 精易_矩形
    .局部变量 有效, 逻辑型, , "2"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    清除数组 (范围数组)
    .计次循环首 (w, x)
        有效 [2] ＝ 假
        .计次循环首 (h, y)
            rgb ＝ 取位图颜色 (位图, x － 1, y － 1)
            .如果真 (rgb ≠ #白色)
                .如果 (有效 [1])
                    有效 [2] ＝ 真
                    范围.顶边 ＝ 选择 (y ＜ 范围.顶边 ＋ 1, y － 1, 范围.顶边)
                .否则
                    有效 [1] ＝ 真
                    有效 [2] ＝ 真
                    范围.左边 ＝ x － 1
                    范围.顶边 ＝ y － 1
                .如果结束
                跳出循环 ()
            .如果真结束
            
        .计次循环尾 ()
        .如果真 (有效 [1])
            .如果真 (有效 [2] ＝ 假)
                有效 [1] ＝ 假
                范围.右边 ＝ x － 范围.左边
                范围.底边 ＝ h － 范围.顶边
                .计次循环首 (范围.底边, y)
                    .计次循环首 (范围.右边, i)
                        rgb ＝ 取位图颜色 (位图, 范围.左边 ＋ i － 2, h － y)
                        .如果真 (rgb ≠ #白色)
                            跳出循环 ()
                        .如果真结束
                        
                    .计次循环尾 ()
                    .如果真 (范围.右边 ＋ 1 ≠ i)
                        跳出循环 ()
                    .如果真结束
                    
                .计次循环尾 ()
                范围.底边 ＝ 范围.底边 ＋ 1 － y
                加入成员 (范围数组, 范围)
            .如果真结束
            
        .如果真结束
        处理事件 ()
    .计次循环尾 ()
    返回 (取数组成员数 (范围数组))

.子程序 位图分割2, 整数型, 公开
    .参数 位图, 字节集
    .参数 点阵数组, 点阵, 参考 可空 数组
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 i, 整数型
    .局部变量 rgb, 整数型
    .局部变量 颜色表, 整数型, , "0"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    清除数组 (点阵数组)
    取位图颜色表 (位图, 颜色表)
    .计次循环首 (w, x)
        .计次循环首 (h, y)
            rgb ＝ 颜色表 [x] [y]
            .如果真 (rgb ≠ #白色)
                加入成员 (点阵数组, 枚举连续点 (颜色表, x, y))
            .如果真结束
            
        .计次循环尾 ()
        处理事件 ()
    .计次循环尾 ()
    返回 (取数组成员数 (点阵数组))

.子程序 取位图有效范围, 精易_矩形, 公开
    .参数 位图, 字节集
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 x, 整数型
    .局部变量 y, 整数型
    .局部变量 有效范围, 精易_矩形
    .局部变量 是否有效, 整数型, , "2,3"

    w ＝ 取字节集数据 (取字节集中间 (位图, 19, 4), 3, )
    h ＝ 取字节集数据 (取字节集中间 (位图, 23, 4), 3, )
    .计次循环首 (w, x)
        是否有效 [1] [2] ＝ 0
        是否有效 [2] [2] ＝ 0
        是否有效 [1] [3] ＝ 是否有效 [1] [1]
        是否有效 [2] [3] ＝ 是否有效 [2] [1]
        .计次循环首 (h, y)
            .如果真 (是否有效 [1] [2] ＝ 0 且 有效范围.左边 ＝ 0)
                .如果真 (取位图颜色 (位图, x － 1, y － 1) ＝ #黑色)
                    是否有效 [1] [1] ＝ 是否有效 [1] [1] ＋ 1
                    是否有效 [1] [2] ＝ 1
                .如果真结束
                
            .如果真结束
            .如果真 (是否有效 [2] [2] ＝ 0 且 有效范围.右边 ＝ 0)
                .如果真 (取位图颜色 (位图, w － x, h － y) ＝ #黑色)
                    是否有效 [2] [1] ＝ 是否有效 [2] [1] ＋ 1
                    是否有效 [2] [2] ＝ 1
                .如果真结束
                
            .如果真结束
            
        .计次循环尾 ()
        是否有效 [1] [1] ＝ 选择 (是否有效 [1] [3] ＝ 是否有效 [1] [1], 0, 是否有效 [1] [1])
        是否有效 [2] [1] ＝ 选择 (是否有效 [2] [3] ＝ 是否有效 [2] [1], 0, 是否有效 [2] [1])
        .如果真 (是否有效 [1] [1] ＝ 2 且 有效范围.左边 ＝ 0)
            有效范围.左边 ＝ x － 2
        .如果真结束
        .如果真 (是否有效 [2] [1] ＝ 2 且 有效范围.右边 ＝ 0)
            有效范围.右边 ＝ x － 2
        .如果真结束
        处理事件 ()
    .计次循环尾 ()
    w ＝ w － 有效范围.左边 － 有效范围.右边
    数组清零 (是否有效)
    .计次循环首 (h, y)
        是否有效 [1] [2] ＝ 0
        是否有效 [2] [2] ＝ 0
        是否有效 [1] [3] ＝ 是否有效 [1] [1]
        是否有效 [2] [3] ＝ 是否有效 [2] [1]
        .计次循环首 (w, x)
            .如果真 (是否有效 [1] [2] ＝ 0 且 有效范围.顶边 ＝ 0)
                .如果真 (取位图颜色 (位图, 有效范围.左边 ＋ x － 1, y － 1) ＝ #黑色)
                    是否有效 [1] [1] ＝ 是否有效 [1] [1] ＋ 1
                    是否有效 [1] [2] ＝ 1
                .如果真结束
                
            .如果真结束
            .如果真 (是否有效 [2] [2] ＝ 0 且 有效范围.底边 ＝ 0)
                .如果真 (取位图颜色 (位图, 有效范围.左边 ＋ w － x, h － y) ＝ #黑色)
                    是否有效 [2] [1] ＝ 是否有效 [2] [1] ＋ 1
                    是否有效 [2] [2] ＝ 1
                .如果真结束
                
            .如果真结束
            
            
        .计次循环尾 ()
        是否有效 [1] [1] ＝ 选择 (是否有效 [1] [3] ＝ 是否有效 [1] [1], 0, 是否有效 [1] [1])
        是否有效 [2] [1] ＝ 选择 (是否有效 [2] [3] ＝ 是否有效 [2] [1], 0, 是否有效 [2] [1])
        .如果真 (是否有效 [1] [1] ＝ 2 且 有效范围.顶边 ＝ 0)
            有效范围.顶边 ＝ y － 2
        .如果真结束
        .如果真 (是否有效 [2] [1] ＝ 2 且 有效范围.底边 ＝ 0)
            有效范围.底边 ＝ y － 2
        .如果真结束
        处理事件 ()
    .计次循环尾 ()
    有效范围.右边 ＝ w
    有效范围.底边 ＝ h － 有效范围.顶边 － 有效范围.底边
    返回 (有效范围)

.子程序 点是否孤立, 整数型, , 0表示孤立,1表示左边,2表示左上,4表示上,8表示右上,16表示右,32表示右下,64表示下,128表示左下
    .参数 位图, 字节集
    .参数 x, 整数型
    .参数 y, 整数型
    .局部变量 相关点, 整数型
    .局部变量 rgb, 整数型

    rgb ＝ 取位图颜色 (位图, x, y)
    .如果 (rgb ≠ #白色)
        rgb ＝ 取位图颜色 (位图, x － 1, y)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 1
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x － 1, y － 1)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 2
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x, y － 1)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 4
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x ＋ 1, y － 1)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 8
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x ＋ 1, y)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 16
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x ＋ 1, y ＋ 1)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 32
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x, y ＋ 1)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 64
        .如果真结束
        rgb ＝ 取位图颜色 (位图, x － 1, y ＋ 1)
        .如果真 (rgb ≠ -1 且 rgb ≠ #白色)
            相关点 ＝ 相关点 ＋ 128
        .如果真结束
        
    .否则
        相关点 ＝ -1
    .如果结束
    返回 (相关点)

.子程序 枚举连续点, 点阵
    .参数 颜色表, 整数型, 参考 数组
    .参数 x, 整数型
    .参数 y, 整数型
    .局部变量 点阵, 点阵
    .局部变量 点组, 整数型, , "0"
    .局部变量 颜色组, 整数型, , "0"
    .局部变量 data, 字节集
    .局部变量 i, 整数型
    .局部变量 x0, 字节集
    .局部变量 y0, 字节集
    .局部变量 minx, 字节集
    .局部变量 maxx, 字节集
    .局部变量 miny, 字节集
    .局部变量 maxy, 字节集

    不重复加入点 (点组, 合并整数 (x, y))
    加入成员 (颜色组, 颜色表 [x] [y])
    子枚举 (颜色表, x, y, 点组, 颜色组)
    data ＝ 指针到字节集 (lstrcpynA_整数数组 (点组, 点组, 0), 取数组成员数 (点组) × 4)
    minx ＝ 取字节集左边 (data, 2)
    maxx ＝ minx
    miny ＝ 取字节集中间 (data, 3, 2)
    maxy ＝ miny
    .计次循环首 (取数组成员数 (点组), i)
        x0 ＝ 取字节集中间 (data, (i － 1) × 4 ＋ 1, 2)
        y0 ＝ 取字节集中间 (data, (i － 1) × 4 ＋ 3, 2)
        minx ＝ 比较字节数小 (minx, x0)
        maxx ＝ 比较字节数大 (maxx, x0)
        miny ＝ 比较字节数小 (miny, y0)
        maxy ＝ 比较字节数大 (maxy, y0)
    .计次循环尾 ()
    重定义数组 (点阵.点, 假, 取字节集数据 (maxx, 3, ) ＋ 1 － 取字节集数据 (minx, 3, ), 取字节集数据 (maxy, 3, ) ＋ 1 － 取字节集数据 (miny, 3, ))
    写到内存 (取重复字节集 (取数组成员数 (点阵.点) × 4, { 255, 255, 255, 0 }), lstrcpynA_整数数组 (点阵.点, 点阵.点, 0), 取数组成员数 (点阵.点) × 4)
    .计次循环首 (取数组成员数 (点组), i)
        x0 ＝ 取字节集中间 (data, (i － 1) × 4 ＋ 1, 2)
        y0 ＝ 取字节集中间 (data, (i － 1) × 4 ＋ 3, 2)
        x ＝ 取字节集数据 (x0, 3, ) ＋ 1 － 取字节集数据 (minx, 3, )
        y ＝ 取字节集数据 (y0, 3, ) ＋ 1 － 取字节集数据 (miny, 3, )
        点阵.点 [x] [y] ＝ 颜色组 [i]
    .计次循环尾 ()
    返回 (点阵)

.子程序 子枚举
    .参数 颜色表, 整数型, 参考 数组
    .参数 x, 整数型
    .参数 y, 整数型
    .参数 点组, 整数型, 参考 数组
    .参数 颜色组, 整数型, 参考 数组
    .局部变量 w, 整数型
    .局部变量 h, 整数型
    .局部变量 位图, 字节集
    .局部变量 孤立值, 整数型
    .局部变量 方位, 整数型, , "8,3"
    .局部变量 i, 整数型
    .局部变量 点x, 整数型
    .局部变量 点y, 整数型

    位图 ＝ 颜色表构建位图 (颜色表)
    孤立值 ＝ 点是否孤立 (位图, x － 1, y － 1)
    位图 ＝ { }
    颜色表 [x] [y] ＝ #白色
    写到内存 ({ 1, 0, 0, 0, 255, 255, 255, 255, 0, 0, 0, 0, 2, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 4, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 8, 0, 0, 0, 1, 0, 0, 0, 255, 255, 255, 255, 16, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 32, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 128, 0, 0, 0, 255, 255, 255, 255, 1, 0, 0, 0 }, lstrcpynA_整数数组 (方位, 方位, 0), 96)
    w ＝ 取字节集数据 (指针到字节集 (lstrcpynA_整数数组 (颜色表, 颜色表, 0) － 8, 4), 3, )
    h ＝ 取字节集数据 (指针到字节集 (lstrcpynA_整数数组 (颜色表, 颜色表, 0) － 4, 4), 3, )
    .计次循环首 (8, i)
        .如果真 (位与 (孤立值, 方位 [i] [1]) ＝ 方位 [i] [1])
            点x ＝ x ＋ 方位 [i] [2]
            点y ＝ y ＋ 方位 [i] [3]
            .如果真 (点x ＜ 1 或 点y ＜ 1 或 点x ＞ w 或 点y ＞ h)
                到循环尾 ()
            .如果真结束
            .如果真 (不重复加入点 (点组, 合并整数 (点x, 点y)))
                加入成员 (颜色组, 颜色表 [点x] [点y])
                子枚举 (颜色表, 点x, 点y, 点组, 颜色组)
            .如果真结束
            
        .如果真结束
        处理事件 ()
    .计次循环尾 ()

.子程序 比较字节数大, 字节集, , 返回大数的字节
    .参数 数1, 字节集
    .参数 数2, 字节集
    .局部变量 i1, 整数型
    .局部变量 i2, 整数型
    .局部变量 n, 整数型

    i1 ＝ 取字节集长度 (数1)
    i2 ＝ 取字节集长度 (数2)
    .如果真 (i1 ≠ 4)
        数1 ＝ 数1 ＋ 取空白字节集 (4 － i1)
    .如果真结束
    .如果真 (i2 ≠ 4)
        数2 ＝ 数2 ＋ 取空白字节集 (4 － i2)
    .如果真结束
    .计次循环首 (4, n)
        .如果真 (数1 [5 － n] ≠ 数2 [5 － n])
            返回 (选择 (数1 [5 － n] ＞ 数2 [5 － n], 取字节集左边 (数1, i1), 取字节集左边 (数2, i2)))
        .如果真结束
        
    .计次循环尾 ()
    返回 (取字节集左边 (数1, i1))

.子程序 比较字节数小, 字节集, , 返回小数的字节
    .参数 数1, 字节集
    .参数 数2, 字节集
    .局部变量 i1, 整数型
    .局部变量 i2, 整数型
    .局部变量 n, 整数型

    i1 ＝ 取字节集长度 (数1)
    i2 ＝ 取字节集长度 (数2)
    .如果真 (i1 ≠ 4)
        数1 ＝ 数1 ＋ 取空白字节集 (4 － i1)
    .如果真结束
    .如果真 (i2 ≠ 4)
        数2 ＝ 数2 ＋ 取空白字节集 (4 － i2)
    .如果真结束
    .计次循环首 (4, n)
        .如果真 (数1 [5 － n] ≠ 数2 [5 － n])
            返回 (选择 (数1 [5 － n] ＜ 数2 [5 － n], 取字节集左边 (数1, i1), 取字节集左边 (数2, i2)))
        .如果真结束
        
    .计次循环尾 ()
    返回 (取字节集左边 (数1, i1))

.子程序 不重复加入点, 逻辑型
    .参数 点组, 整数型, 参考 数组
    .参数 点, 整数型, , 低16位X,高16位Y
    .局部变量 data, 字节集
    .局部变量 i, 整数型

    data ＝ 指针到字节集 (lstrcpynA_整数数组 (点组, 点组, 0), 取数组成员数 (点组) × 4)
    .循环判断首 ()
        i ＝ i ＋ 1
        i ＝ 寻找字节集 (data, 到字节集 (点), i)
        .如果真 (i ＝ -1)
            加入成员 (点组, 点)
            返回 (真)
        .如果真结束
        处理事件 ()
    .循环判断尾 (到整数 ((i － 1) ÷ 4) ≠ (i － 1) ÷ 4)
    返回 (假)

